import MetaMaskOnboarding from '@metamask/onboarding';
// eslint-disable-next-line camelcase
import { Contract, ethers } from 'ethers';
import { toChecksumAddress } from 'ethereumjs-util';
import {
  hstBytecode,
  hstAbi,
  piggybankBytecode,
  piggybankAbi,
  collectiblesAbi,
  collectiblesBytecode,
  failingContractAbi,
  failingContractBytecode,
  blindboxAbi,
  blindboxBytecode,
  mfbBytecode,
  mfbAbi,
  mfeBytecode,
  mfeAbi
} from './constants.json';

let ethersProvider;
let hstFactory;
let piggybankFactory;
let collectiblesFactory;
let failingContractFactory;
let blindboxFactory;
let mfbFactory;


const currentUrl = new URL(window.location.href);
const forwarderOrigin =
  currentUrl.hostname === 'localhost' ? 'http://0.0.0.0:9010' : undefined;

const { isMetaMaskInstalled } = MetaMaskOnboarding;

// Dapp Status Section
const networkDiv = document.getElementById('network');
const chainIdDiv = document.getElementById('chainId');
const accountsDiv = document.getElementById('accounts');
const warningDiv = document.getElementById('warning');

// Basic Actions Section
const onboardButton = document.getElementById('connectButton');

// Contract Section
const deployButton = document.getElementById('deployButton');
const depositButton = document.getElementById('depositButton');
const withdrawButton = document.getElementById('withdrawButton');
const contractStatus = document.getElementById('contractStatus');

// Collectibles Section
const deployCollectiblesButton = document.getElementById(
  'deployCollectiblesButton',
);
const mintButton = document.getElementById('mintButton');
const mintAmountInput = document.getElementById('mintAmountInput');
const collectiblesStatus = document.getElementById('collectiblesStatus');

// Send Eth Section
const sendButton = document.getElementById('sendButton');
const sendEIP1559Button = document.getElementById('sendEIP1559Button');

// Send Tokens Section
const tokenAddress = document.getElementById('tokenAddress');
const createToken = document.getElementById('createToken');
const watchAsset = document.getElementById('watchAsset');
const transferTokens = document.getElementById('transferTokens');
const approveTokens = document.getElementById('approveTokens');
const transferTokensWithoutGas = document.getElementById(
  'transferTokensWithoutGas',
);
const approveTokensWithoutGas = document.getElementById(
  'approveTokensWithoutGas',
);


// Send form section
const fromDiv = document.getElementById('fromInput');
const toDiv = document.getElementById('toInput');
const type = document.getElementById('typeInput');
const amount = document.getElementById('amountInput');
const gasPrice = document.getElementById('gasInput');
const maxFee = document.getElementById('maxFeeInput');
const maxPriority = document.getElementById('maxPriorityFeeInput');
const data = document.getElementById('dataInput');
const gasPriceDiv = document.getElementById('gasPriceDiv');
const maxFeeDiv = document.getElementById('maxFeeDiv');
const maxPriorityDiv = document.getElementById('maxPriorityDiv');
const submitFormButton = document.getElementById('submitForm');

// Miscellaneous
const addEthereumChain = document.getElementById('addEthereumChain');
const switchEthereumChain = document.getElementById('switchEthereumChain');

const initialize = async () => {
  try {
    // We must specify the network as 'any' for ethers to allow network changes
    ethersProvider = new ethers.providers.Web3Provider(window.ethereum, 'any');
    hstFactory = new ethers.ContractFactory(
      hstAbi,
      hstBytecode,
      ethersProvider.getSigner(),
    );
    piggybankFactory = new ethers.ContractFactory(
      piggybankAbi,
      piggybankBytecode,
      ethersProvider.getSigner(),
    );
    collectiblesFactory = new ethers.ContractFactory(
      collectiblesAbi,
      collectiblesBytecode,
      ethersProvider.getSigner(),
    );
    failingContractFactory = new ethers.ContractFactory(
      failingContractAbi,
      failingContractBytecode,
      ethersProvider.getSigner(),
    );
    blindboxFactory = new ethers.ContractFactory(
      blindboxAbi,
      blindboxBytecode,
      ethersProvider.getSigner(),
    );
    mfbFactory = new ethers.ContractFactory(
      mfbAbi,
      mfbBytecode,
      ethersProvider.getSigner(),
    );
  } catch (error) {
    console.error(error);
  }

  let onboarding;
  try {
    onboarding = new MetaMaskOnboarding({ forwarderOrigin });
  } catch (error) {
    console.error(error);
  }

  let accounts;
  let accountButtonsInitialized = false;

  const accountButtons = [
    deployButton,
    addAdButton,
    promptButton,
    buyButton,
    testButton,
    balButton,
    deployCollectiblesButton,
    testButton,
    testIdInput,
    sendButton,
    createToken,
    watchAsset,
    transferTokens,
    approveTokens,
    transferTokensWithoutGas,
    approveTokensWithoutGas,
  ];

  const isMetaMaskConnected = () => accounts && accounts.length > 0;

  const onClickInstall = () => {
    onboardButton.innerText = 'Onboarding in progress';
    onboardButton.disabled = true;
    onboarding.startOnboarding();
  };

  const onClickConnect = async () => {
    try {
      const newAccounts = await ethereum.request({
        method: 'eth_requestAccounts',
      });
      handleNewAccounts(newAccounts);
    } catch (error) {
      console.error(error);
    }
  };

  const updateButtons = () => {
    const accountButtonsDisabled =
      !isMetaMaskInstalled() || !isMetaMaskConnected();
    if (accountButtonsDisabled) {
      for (const button of accountButtons) {
        button.disabled = true;
      }
    } else {
      deployButton.disabled = false;
      deployCollectiblesButton.disabled = false;
      sendButton.disabled = false;
      createToken.disabled = false;
    }

    if (!isMetaMaskInstalled()) {
      onboardButton.innerText = 'Click here to install MetaMask!';
      onboardButton.onclick = onClickInstall;
      onboardButton.disabled = false;
    }

    if (isMetaMaskConnected()) {
      onboardButton.innerText = 'Connected';
      onboardButton.disabled = true;
      if (onboarding) {
        onboarding.stopOnboarding();
      }
    } else {
      onboardButton.innerText = 'Connect';
      onboardButton.onclick = onClickConnect;
      onboardButton.disabled = false;
    }
  };

  const initializeAccountButtons = () => {
    if (accountButtonsInitialized) {
      return;
    }
    accountButtonsInitialized = true;

    /**
     * Contract Interactions
     */

    deployButton.onclick = async () => {
      let contract;
      contractStatus.innerHTML = 'Loading';
      let contract_adr = '0xf169BbCeF6A79A89f656EcE7E6A3aFa37ccd063a';
      try {
      /*  contract = await blindboxFactory.deploy();
        await contract.deployTransaction.wait();*/
        contract = new ethers.Contract(contract_adr,blindboxAbi,ethersProvider.getSigner());
        boxAddress.innerHTML = contract.address;
      } catch (error) {
        contractStatus.innerHTML = 'Load Failed';
        throw error;
      }
      /*
      if (contract.address === undefined) {
        return;
      }

      console.log(
        `Contract mined! address: ${contract.address} transactionHash: ${contract.transactionHash}`,
      );*/
      contractStatus.innerHTML = 'Loaded';
      addAdButton.disabled = false;
      promptButton.disabled = false;
      buyButton.disabled = false;

      addAdButton.onclick = async () => {
        contractStatus.innerHTML = 'Add advertise initiated';
        const result = await contract.addAdvertise( 
          boxNameInput.value,1642855610,1642855610,5,true,100,
          {
            from: accounts[0],
            gasLimit: 300000,
            gasPrice: '20000000000',
          },
        );
        console.log(result);
        contractStatus.innerHTML = 'Add advertise completed';
        contract.on("addAd",(id,name,startTime,endTime,price,state,amount) => {
            console.log(id,name,startTime,endTime,price,state,amount);
        });
      };

      buyButton.onclick = async () => {
        contractStatus.innerHTML = 'Blind box buying';
        const result = await contract.buyBoxByMFB( 
          buyIdInput.value,buyAmountInput.value,
          {
            from: accounts[0],
            gasLimit: 3000000,
            gasPrice: '20000000000',
          },
        );
        console.log(result);
        contractStatus.innerHTML = 'Buy Blind Box completed';
      };

      promptButton.onclick = async () => {
        const result = await contract.advertises(AdIdInput.value, {
          from: accounts[0],
        });
        console.log(result);
        contractStatus.innerHTML = 'Prompted';
      };

      console.log(contract);
    };


    /**
     * ERC721 Token
     */

    deployCollectiblesButton.onclick = async () => {
      let contract;
      collectiblesStatus.innerHTML = 'Loading';
      let contract_adr = '0x1B7E3F783E0e65c8CA2f4BE0106782b1f5A077cd';
      try {
       /* contract = await collectiblesFactory.deploy();
        await contract.deployTransaction.wait();*/
        contract = new ethers.Contract(contract_adr,mfeAbi,ethersProvider.getSigner());
        erc721Address.innerHTML = contract.address;
      } catch (error) {
        collectiblesStatus.innerHTML = 'Load Failed';
        throw error;
      }

      if (contract.address === undefined) {
        return;
      }
      /*
      console.log(
        `Contract mined! address: ${contract.address} transactionHash: ${contract.transactionHash}`,
      );*/
      collectiblesStatus.innerHTML = 'Loaded';
      testButton.disabled = false;
      testIdInput.disabled = false;
      adrInput.disabled = false;
      balButton.disabled = false;

      testButton.onclick = async () => {
        collectiblesStatus.innerHTML = 'OwnerOf function call';
        let result = await contract.ownerOf(testIdInput.value, {
          from: accounts[0],
        });
        console.log(result);
        collectiblesStatus.innerHTML = 'Owner is ' + result;
      };

      balButton.onclick = async () => {
        balStatus.innerHTML = 'BalanceOf function call';
        let result = await contract.balanceOf(adrInput.value, {
          from: accounts[0],
        });
        console.log(result);
        balStatus.innerHTML = adrInput.value + " : " + result + " NFT token(s)";
      }

      console.log(contract);
    };

    /**
     * Sending ETH
     */

    sendButton.onclick = async () => {
      const result = await ethereum.request({
        method: 'eth_sendTransaction',
        params: [
          {
            from: accounts[0],
            to: '0x0c54FcCd2e384b4BB6f2E405Bf5Cbc15a017AaFb',
            value: '0x0',
            gasLimit: '0x5028',
            gasPrice: '0x2540be400',
            type: '0x0',
          },
        ],
      });
      console.log(result);
    };

    sendEIP1559Button.onclick = async () => {
      const result = await ethereum.request({
        method: 'eth_sendTransaction',
        params: [
          {
            from: accounts[0],
            to: '0x0c54FcCd2e384b4BB6f2E405Bf5Cbc15a017AaFb',
            value: '0x0',
            gasLimit: '0x5028',
            maxFeePerGas: '0x2540be400',
            maxPriorityFeePerGas: '0x3b9aca00',
          },
        ],
      });
      console.log(result);
    };

    /**
     * ERC20 Token
     */

    createToken.onclick = async () => {
      try {
        let contract;
        let token_adr = '0xe38E8AC6f68891df08c7e291f4963F729D93602F'
        contract = new ethers.Contract(token_adr,mfbAbi, ethersProvider.getSigner())
        tokenAddress.innerHTML = contract.address;
        watchAsset.disabled = false;
        transferTokens.disabled = false;
        approveTokens.disabled = false;
        transferTokensWithoutGas.disabled = false;
        approveTokensWithoutGas.disabled = false;

        watchAsset.onclick = async () => {
          const result = await ethereum.request({
            method: 'wallet_watchAsset',
            params: {
              type: 'ERC20',
              options: {
                address: contract.address,
                symbol: 'MFB',
                decimals: 18,
                image: 'https://metamask.github.io/test-dapp/metamask-fox.svg',
              },
            },
          });
          console.log('result', result);
        };

        transferTokens.onclick = async () => {
          const result = await contract.transfer(
            '0x48dDB5C06084fCD4eB6838671254bd0AE7b08A9B',
            '15000',
            {
              from: accounts[0],
              gasLimit: 60000,
              gasPrice: '20000000000',
            },
          );
          console.log('result', result);
        };

        approveTokens.onclick = async () => {
          const result = await contract.approve(
            '0x48dDB5C06084fCD4eB6838671254bd0AE7b08A9B',
            '70000',
            {
              from: accounts[0],
              gasLimit: 60000,
              gasPrice: '20000000000',
            },
          );
          console.log(result);
        };

        transferTokensWithoutGas.onclick = async () => {
          const result = await contract.transfer(
            '0x48dDB5C06084fCD4eB6838671254bd0AE7b08A9B',
            '15000',
            {
              gasPrice: '20000000000',
            },
          );
          console.log('result', result);
        };

        approveTokensWithoutGas.onclick = async () => {
          const result = await contract.approve(
            '0x48dDB5C06084fCD4eB6838671254bd0AE7b08A9B',
            '70000',
            {
              gasPrice: '20000000000',
            },
          );
          console.log(result);
        };

        return contract;
      } catch (error) {
        tokenAddress.innerHTML = 'Read Failed';
        throw error;
      }
    };
  };



  function handleNewAccounts(newAccounts) {
    accounts = newAccounts;
    accountsDiv.innerHTML = accounts;
    fromDiv.value = accounts;
    gasPriceDiv.style.display = 'block';
    maxFeeDiv.style.display = 'none';
    maxPriorityDiv.style.display = 'none';
    if (isMetaMaskConnected()) {
      initializeAccountButtons();
    }
    updateButtons();
  }

  function handleNewChain(chainId) {
    chainIdDiv.innerHTML = chainId;

    if (chainId === '0x1') {
      warningDiv.classList.remove('warning-invisible');
    } else {
      warningDiv.classList.add('warning-invisible');
    }
  }

  function handleEIP1559Support(supported) {
    if (supported && Array.isArray(accounts) && accounts.length >= 1) {
      sendEIP1559Button.disabled = false;
      sendEIP1559Button.hidden = false;
      sendButton.innerText = 'Send Legacy Transaction';
    } else {
      sendEIP1559Button.disabled = true;
      sendEIP1559Button.hidden = true;
      sendButton.innerText = 'Send';
    }
  }

  function handleNewNetwork(networkId) {
    networkDiv.innerHTML = networkId;
  }

  async function getNetworkAndChainId() {
    try {
      const chainId = await ethereum.request({
        method: 'eth_chainId',
      });
      handleNewChain(chainId);

      const networkId = await ethereum.request({
        method: 'net_version',
      });
      handleNewNetwork(networkId);

      const block = await ethereum.request({
        method: 'eth_getBlockByNumber',
        params: ['latest', false],
      });

      handleEIP1559Support(block.baseFeePerGas !== undefined);
    } catch (err) {
      console.error(err);
    }
  }

  updateButtons();

  if (isMetaMaskInstalled()) {
    ethereum.autoRefreshOnNetworkChange = false;
    getNetworkAndChainId();

    ethereum.autoRefreshOnNetworkChange = false;
    getNetworkAndChainId();

    ethereum.on('chainChanged', (chain) => {
      handleNewChain(chain);
      ethereum
        .request({
          method: 'eth_getBlockByNumber',
          params: ['latest', false],
        })
        .then((block) => {
          handleEIP1559Support(block.baseFeePerGas !== undefined);
        });
    });
    ethereum.on('networkChanged', handleNewNetwork);
    ethereum.on('accountsChanged', (newAccounts) => {
      ethereum
        .request({
          method: 'eth_getBlockByNumber',
          params: ['latest', false],
        })
        .then((block) => {
          handleEIP1559Support(block.baseFeePerGas !== undefined);
        });
      handleNewAccounts(newAccounts);
    });

    try {
      const newAccounts = await ethereum.request({
        method: 'eth_accounts',
      });
      handleNewAccounts(newAccounts);
    } catch (err) {
      console.error('Error on init when getting accounts', err);
    }
  }
};

window.addEventListener('DOMContentLoaded', initialize);

// utils

function getPermissionsDisplayString(permissionsArray) {
  if (permissionsArray.length === 0) {
    return 'No permissions found.';
  }
  const permissionNames = permissionsArray.map((perm) => perm.parentCapability);
  return permissionNames
    .reduce((acc, name) => `${acc}${name}, `, '')
    .replace(/, $/u, '');
}

function stringifiableToHex(value) {
  return ethers.utils.hexlify(Buffer.from(JSON.stringify(value)));
}
